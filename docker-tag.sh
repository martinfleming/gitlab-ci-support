#!/bin/bash

set -e

# Assume this script is in the src directory and work from that location
PROJECT_ROOT="$(cd "$(dirname "$0")" && pwd)"

source "$PROJECT_ROOT/include/tag.sh"

docker login "$CI_REGISTRY" -u "$CI_REGISTRY_USER" -p "$CI_JOB_TOKEN"
docker pull "$CI_REGISTRY_IMAGE":latest
docker tag "$CI_REGISTRY_IMAGE":latest "$CI_REGISTRY_IMAGE":"$NEW_TAG"
docker push "$CI_REGISTRY_IMAGE":"$NEW_TAG"
