#!/bin/bash

set -e

# Assume this script is in the src directory and work from that location
PROJECT_ROOT="$(cd "$(dirname "$0")" && pwd)"

source "$PROJECT_ROOT/include/tag.sh"

GIT_URL="${CI_PROJECT_URL/https\:\/\//}"

git tag "v$NEW_TAG"
git push "https://$CI_DEPLOY_USER:$ACCESS_TOKEN@$GIT_URL.git" --tags
