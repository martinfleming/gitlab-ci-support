# Gitlab CI Support

This repo provides shared scripts for common functionality within Gitlab CI.

It is recommended that you set your **Git strategy** to clone mode to ensure repo tags exist in  
`Settings -> CI / CD -> General pipelines`

For gitlab runner to have write access to your git repository to push back tags you will need to generate an access_token `User Settings -> Access Tokens` with the scopes `read_repository` and `write_repository`.  

Take the value of this token and add it as a variable in your project settings `Settings -> CI / CD -> General pipelines` as `ACCESS_TOKEN`.  I advise creating this variable as both protected and masked.

## Example `gitlab-ci.yml` file

As an example of how to use the scripts provided, please see the `gitlab-ci-yaml` file below.

```yaml
image: docker:19.03.0

variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: "/certs"
    SUPPORT_REPO: "https://gitlab.com/martinfleming/gitlab-ci-support.git"

services:
    - docker:19.03.0-dind

stages:
    - build
    - tag

# build docker image
build:
    stage: build
    tags:
        - docker
    script:
        - apk update && apk add bash git
        - git clone "$SUPPORT_REPO" --depth 1
        - bash gitlab-ci-support/docker-build.sh
        - rm -Rf gitlab-ci-support
    except:
        - /-backup$/
        - /-wip$/

# tag release
tag:
    stage: tag
    tags:
        - docker
    script:
        - apk update && apk add bash git
        - git clone "$SUPPORT_REPO" --depth 1
        - bash gitlab-ci-support/git-tag.sh patch
        - bash gitlab-ci-support/docker-tag.sh keep
        - rm -Rf gitlab-ci-support
    only:
        - /^master$/
```
